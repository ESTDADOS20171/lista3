package hash

class ListaEncadeada[E <% Ordered[E]]() {

  class No(valor: E, proximo: No = null, anterior: No = null) {

    var v: E = valor;
    var p: No = proximo;
    var a: No = anterior;

    override def toString(): String = {
      var str = "";
      if (a != null) {
        str += (a.v + " <- " + v);
      } else {
        str += ("null <- " + v);
      }

      if (p != null) {
        str += (" -> " + p.v);
      } else {
        str += (" -> null");
      }

      return str;
    }

  }

  protected var cabeca: No = null;

  def inserir(e: E): Boolean = {
    var no: No = new No(e, null);
    no.p = cabeca;
    cabeca = no;
    return true;
  }

  def estaVazia(): Int = if (cabeca == null) return 1 else return 0;

  def imprimeValores() {
    print("[")
    var iterador = cabeca

    while (iterador != null) {
      imprimeNo(iterador)
      iterador = iterador.p;
    }

    print("]\n")
  }

  private def imprimeNo(n: No, reverso: Boolean = false) {

    if (n == null) {
      return
    }

    if (n.p != null && reverso) {
      print(", ")
    }

    print(n.v)

    if (n.p != null && !reverso) {
      print(", ")
    }

  }

  def imprimeValoresRecursao(comeco: Boolean = true, no: No = null) {
    if (comeco) {
      print("[")

      imprimeNo(cabeca)

      if (cabeca.p != null) {
        imprimeValoresRecursao(false, cabeca.p)
      }

    } else {

      imprimeNo(no)

      if (no.p != null) {
        imprimeValoresRecursao(false, no.p)
      }
    }

    if (comeco) {
      print("]\n")
    }

  }

  def imprimeValoresRecursaoReversa(comeco: Boolean = true, no: No = null) {
    if (comeco) {
      print("[")

      if (cabeca != null && cabeca.p != null) {
        imprimeValoresRecursaoReversa(false, cabeca.p)

      }

      imprimeNo(cabeca, true)

    } else {
      if (no != null && no.p != null) {
        imprimeValoresRecursaoReversa(false, no.p)

      }

      imprimeNo(no, true)

    }

    if (comeco) {
      print("]\n")
    }
  }

  def recuperar(indice: Int): Option[E] = {
    var iterador = cabeca;
    var contador = 0;

    while (iterador != null) {
      if (contador == indice) {
        return Some(iterador.v);
      }

      iterador = iterador.p;
      contador = contador + 1;
    }

    return None;

  }

  def buscar(valor: E): Option[Int] = {
    var iterador = cabeca;
    var contador = 0;

    while (iterador != null) {
      if (iterador.v == valor) {
        return Some(contador);
      }

      iterador = iterador.p;
      contador = contador + 1;
    }

    return None;

  }

  def remover(valor: E): Option[E] = {

    if (this.estaVazia() != 1) {

      if (cabeca.v == valor) {

        cabeca = cabeca.p;

      } else {
        var iterador = cabeca;
        var anterior = cabeca;

        while (iterador != null) {
          if (iterador.v == valor) {

            anterior.p = iterador.p;

            return Some(iterador.v);
          }

          anterior = iterador;
          iterador = iterador.p;

        }
      }

    }

    return None;
  }

  def removerRecursivo(valor: E, anterior: No = null, atual: No = null): Option[E] = {

    if (this.estaVazia() != 1) {

      if (valor == cabeca.v) {

        cabeca = cabeca.p;

      } else if (atual != null) {

        if (valor == atual.v) {
          anterior.p = atual.p;
          return Some(atual.v);
        } else {
          return removerRecursivo(valor, atual, atual.p);
        }
      }

    }

    return None;
  }

  def liberar() {
    cabeca = null
  }

  override def toString(): String = {
    if (this.estaVazia() == 1) {
      return "[]";
    } else {

      var str = "[";
      var iterador = cabeca;
      while (iterador != null) {

        str = str + iterador.v;

        if (iterador.p != null) {
          str = str + ", ";
        }

        iterador = iterador.p;
      }

      return str + "]";
    }

  }
}
