package hash

object Questao1 extends App {
  
  var tb = new TabelaHash[Int](10);
  
  
  println("Tabela Hash: \n" + tb);
  
  
  println("Inserir 10.");
  tb.inserir(10);
  println("Tabela Hash: \n" + tb);
  
  println("Inserir 11.");
  tb.inserir(11);
  println("Tabela Hash: \n" + tb);
  
  println("Inserir 21.");
  tb.inserir(21);
  println("Tabela Hash: \n" + tb);
  
  println("Inserir 22.");
  tb.inserir(22);
  println("Tabela Hash: \n" + tb);
  
  println("Inserir 333.");
  tb.inserir(33);
  println("Tabela Hash: \n" + tb);
  
  
  println("Inserir -13.");
  tb.inserir(-13);
  println("Tabela Hash: \n" + tb);
  
  println("Inserir -4.");
  tb.inserir(-4);
  println("Tabela Hash: \n" + tb);
  
  
  println("Tem 10? " + tb.buscar(10));
  println("Tem 11? " + tb.buscar(11));
  println("Tem 4? " + tb.buscar(4));
  println("Tem -4? " + tb.buscar(-4));
  
  
  println("\nTabela Hash: \n" + tb);
  println("Remover 10.");
  tb.remover(10);
  
  println("Tem 10? " + tb.buscar(10));
  println("Tabela Hash: \n" + tb);
  
  
}