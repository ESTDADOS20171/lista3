package hash

class TabelaHash[E <% Ordered[E]](n: Int) {
  private var chaves = new Array[ListaEncadeada[E]](n / 2);

  for (i <- 0 until chaves.length) {
    chaves(i) = new ListaEncadeada();
  }

  def tamanho() = chaves.length;

  def inserir(valor: E): Boolean = chaves(h(valor)).inserir(valor);

  def buscar(valor: E): Boolean = chaves(h(valor)).buscar(valor) match {
    case Some(indice) => return (indice > -1);
    case None => return false;
  };

  def remover(valor: E): Option[E] = chaves(h(valor)).remover(valor);

  // minha funcao de hash
  private def h(valor: E) = scala.math.abs(valor.hashCode()) % chaves.length;

  def liberar() {
    chaves = null;
  }

 
  override def toString(): String = {

    var strArray = for ((l, c) <- chaves.zipWithIndex) yield ("#" + c + ": " + l + "\n");

    return strArray.mkString;
  }
}